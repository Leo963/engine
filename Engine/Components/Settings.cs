﻿using System;
using System.Windows.Forms;

namespace Engine.Components
{
    class Settings
    {
        public static void EngineSetup()
        {
            try
            {
                IniReader ir = new IniReader("props.ini");
                Window.msaa = int.Parse(ir.Read("MSAA", "Graphics"));
                Window.shadowRes = int.Parse(ir.Read("ShadowResolution", "Graphics"));
            }
            catch (Exception)
            {
                MessageBox.Show("Cannot read props file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
