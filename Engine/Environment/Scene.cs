﻿using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using Engine.Renderer;
using Engine.Components;
using Engine.Input;
using OpenTK.Graphics;

namespace Engine.Environment
{
    class Scene
    {
        readonly string name;
        Skybox skybox;
        public static Matrix4 projectionMatrix;
        public static List<Light> lights = new List<Light>();
        Render rnd = new Render();
        Camera cam = new Camera();
        Keyboard keyboard = new Keyboard();
        List<Mesh> meshes = new List<Mesh>();
        Vector2 lastMousePos = new Vector2();
        Shader shader = new Shader("lightv", "lightf");
        Shader depthShader = new Shader("depthv", "depthf");
        public bool mouseLock = true;

        public Scene(string name)
        {
            this.name = name;
            skybox = new Skybox();
            lights.Add(new Light(new Vector3(2), new Vector3(1)));
            LoadObjects();
        }

        private void LoadObjects()
        {
            try
            {
                using (StreamReader sr = new StreamReader(@"assets\" + name + ".scn"))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] data = line.Split('/');
                        Mesh mesh = ObjLoader.loadObj(data[1]);
                        mesh.name = data[0];
                        mesh.position = new Vector3(float.Parse(data[2], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[3], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[4], System.Globalization.CultureInfo.InvariantCulture));
                        if (data.Length == 8)
                        {
                            mesh.rotation = new Quaternion(MathHelper.DegreesToRadians(float.Parse(data[5], System.Globalization.CultureInfo.InvariantCulture)), MathHelper.DegreesToRadians(float.Parse(data[6], System.Globalization.CultureInfo.InvariantCulture)), MathHelper.DegreesToRadians(float.Parse(data[7], System.Globalization.CultureInfo.InvariantCulture)));
                        }
                        meshes.Add(mesh);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Cannot load scene", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Environment.Exit(0);
            }
        }

        public void Update(Window window)
        {
            keyboard.HandleCameraMovement(cam, this);
            projectionMatrix = cam.GetViewMatrix() * Matrix4.CreatePerspectiveFieldOfView(1.3f, window.ClientSize.Width / (float)window.ClientSize.Height, 0.01f, 60.0f);
            if (window.Focused)
            {
                Vector2 delta = lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);

                cam.AddRotation(delta.X, delta.Y);
                if (mouseLock)
                    ResetCursor(window);
            }
        }
        public void Draw()
        {
            Light.SetUpLights(shader, lights);
            shader.setVec3("viewPos", cam.Position);
            shader.setMat4("projection", projectionMatrix);
            GL.ClearColor(Color4.CornflowerBlue);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projectionMatrix);
            GL.MatrixMode(MatrixMode.Modelview);
            rnd.RenderMeshes(meshes.ToArray(), shader);
            GL.UseProgram(0);
            skybox.DrawSkybox(rnd, cam.Position);
        }

        public void DrawLight(int lightIdnex)
        {
            depthShader.Use();
            depthShader.setMat4("projection", lights[lightIdnex].lightSpaceMatrix);
            GL.MatrixMode(MatrixMode.Projection);
            Matrix4 tempMat = lights[lightIdnex].lightSpaceMatrix;
            GL.LoadMatrix(ref tempMat);
            GL.ClearColor(Color4.CornflowerBlue);
            rnd.RenderForLightmap(meshes.ToArray(), depthShader);
        }

        public void ResetCursor(Window window)
        {
            OpenTK.Input.Mouse.SetPosition(window.Bounds.Left + window.Bounds.Width / 2, window.Bounds.Top + window.Bounds.Height / 2);
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        public void DebugCommand()
        {
            bool exit = false;
            Console.WriteLine();
            while (!exit)
            {
                Console.Write("Enter debug command: ");
                string cmd = Console.ReadLine();
                switch (cmd)
                {
                    case "model":
                        Console.Write("Enter model name: ");
                        string name = Console.ReadLine();
                        meshes.Add(ObjLoader.loadObj(name));
                        break;
                    case "clearmod":
                        int verts = 0;
                        foreach (var item in meshes)
                        {
                            verts += item.vertices.Length;
                        }
                        Console.WriteLine("Clearing " + meshes.Count + " meshes of total " + verts + " vertices");
                        meshes.Clear();
                        break;
                    case "exit":
                        exit = true;
                        break;
                }
            }
        }
    }

}