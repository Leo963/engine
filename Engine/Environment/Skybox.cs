﻿using System;
using System.Collections.Generic;
using OpenTK;
using Engine.Renderer;
using Engine.Components;

namespace Engine.Environment
{
    class Skybox
    {
        Mesh skybox;
        Shader skyder = new Shader("skyv", "skyf");
        public Skybox()
        {
            skybox = ObjLoader.loadObj("skybox");
        }
        public void DrawSkybox(Render rnd, Vector3 camPosition)
        {
            skyder.Use();
            skyder.setMat4("projection", Environment.Scene.projectionMatrix);
            skyder.setVec3("pos", camPosition);
            rnd.RenderMesh(skybox);
        }
    }
}
