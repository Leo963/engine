#version 330 core
in vec2 UV;
out vec3 color;
uniform sampler2D renderedTexture;

void main(){
	vec3 matColor = texture(renderedTexture, UV).xyz;
	color = matColor;
}