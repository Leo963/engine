#version 330 core
out vec4 FragColor;
#define NR_LIGHTS 20
struct Light {
    vec3 pos;
    vec3 col;
    float range;
    float intensity;
};

uniform sampler2D diffTex;
uniform sampler2D shadowMap[NR_LIGHTS];

in vec2 TexCoords;
in vec3 Normal;  
in vec3 FragPos;
in vec4 FragPosLightSpace[NR_LIGHTS];

uniform Light pointLight[NR_LIGHTS];
uniform vec3 viewPos;
uniform bool hasTexture;
uniform int light_lenght;

float ShadowCalculation(vec4 fragPosLightSpace, int pass, float bias)
{
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    if(projCoords.z > 1.0)
        return 0.0;
    float closestDepth = texture(shadowMap[pass], projCoords.st).z;
    float currentDepth = projCoords.z;
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap[pass], 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap[pass], projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
    return shadow;
}
vec3 CalcLight(Light lt)
{
    float d = 1 - distance(lt.pos, FragPos) / lt.range;
    if (d > 0 )
    {
        //diffuse
        vec3 norm = normalize(Normal);
        vec3 lightDir = normalize(lt.pos - FragPos);
        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuse;
        d *= lt.intensity;
        if (hasTexture)
            diffuse = diff * vec3(texture(diffTex, TexCoords));
        else
            diffuse = diff * lt.col;
    
        // specular
        float specularStrength = 0.5;
        vec3 viewDir = normalize(viewPos - FragPos);
        vec3 reflectDir = reflect(-lightDir, norm);  
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
        vec3 specular = specularStrength * spec * lt.col;
        return (diffuse + specular) * d;
    }
    else
    {
        return vec3(0,0,0);
    }

}
vec3 CalcLightShed(Light lt, int currShad)
{
    vec3 lightDir = normalize(lt.pos - FragPos);
    float ambientStrength = 0.3;
    vec3 ambient;
    if (hasTexture)
        ambient = ambientStrength * vec3(texture(diffTex, TexCoords));
    else
        ambient = ambient * lt.col;
    float bias = max(0.05 * (1.0 - dot(normalize(Normal), lightDir)), 0.005);  
    float shadow = ShadowCalculation(FragPosLightSpace[currShad], currShad, bias);
    return (ambient + (1.0 - shadow));
}
void main()
{
    vec3 result;
    vec3 temp;
    for(int i = 0; i < light_lenght; i++)
        temp += CalcLightShed(pointLight[i], i);
    for(int i = 0; i < light_lenght; i++)
        result += temp * CalcLight(pointLight[i]);
    FragColor = vec4(result, 1.0);
} 