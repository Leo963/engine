#version 330 core
layout (location = 0) in vec3 aPos;
uniform mat4 projection;
uniform vec3 pos;
uniform mat4 rot;

void main()
{
    gl_Position = projection * rot * vec4(aPos + pos, 1.0);
}