﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;

namespace Engine.Renderer
{
    class Shader
    {
        int programID;
        public Shader(string vertexShader, string fragmentShader)
        {
            string fPath = fragmentShader + ".glsl";
            string vPath = vertexShader + ".glsl";
            programID = CompileShaders(vPath,fPath);
        }
        public void Use()
        {
            GL.UseProgram(programID);
        }

        public void setBool(string name, bool value)
        {
            GL.Uniform1(GL.GetUniformLocation(programID, name), Convert.ToInt32(value));
        }
        public void setInt(string name, int value)
        {
            GL.Uniform1(GL.GetUniformLocation(programID, name), value);
        }
        public void setFloat(string name, float value)
        {
            GL.Uniform1(GL.GetUniformLocation(programID, name), value);
        }
        public void setVec2(string name, Vector2 value)
        {
            GL.Uniform2(GL.GetUniformLocation(programID, name), value);
        }
        public void setVec3(string name, Vector3 value)
        {
            GL.Uniform3(GL.GetUniformLocation(programID, name), value);
        }
        public void setVec4(string name, Vector4 value)
        {
            GL.Uniform4(GL.GetUniformLocation(programID, name), value);
        }
        public void setMat2(string name, Matrix2 value)
        {
            GL.UniformMatrix2(GL.GetUniformLocation(programID, name), false, ref value);
        }
        public void setMat3(string name, Matrix3 value)
        {
            GL.UniformMatrix3(GL.GetUniformLocation(programID, name), false, ref value);
        }
        public void setMat4(string name, Matrix4 value)
        {
            GL.UniformMatrix4(GL.GetUniformLocation(programID, name), false, ref value);
        }
        public static int CompileShaders(string vertex, string fragment)
        {
            bool useFragment = false;
            bool useVertex = false;
            if (File.Exists(@"assets\" + fragment))
                useFragment = true;
            if (File.Exists(@"assets\" + vertex))
                useVertex = true;

            var program = GL.CreateProgram();
            int fragmentShader = 0;
            int vertexShader = 0;
            if (useVertex)
            {
                vertexShader = GL.CreateShader(ShaderType.VertexShader);
                GL.ShaderSource(vertexShader,
                File.ReadAllText(@"assets\" + vertex));
                GL.CompileShader(vertexShader);
                GL.AttachShader(program, vertexShader);
            }

            if (useFragment)
            {
                fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
                GL.ShaderSource(fragmentShader,
                File.ReadAllText(@"assets\" + fragment));
                GL.CompileShader(fragmentShader);
                GL.AttachShader(program, fragmentShader);
            }
            GL.LinkProgram(program);
            if (useFragment)
            {
                GL.DetachShader(program, fragmentShader);

                GL.DeleteShader(fragmentShader);
            }  
            if (useVertex)
            {
                GL.DetachShader(program, vertexShader);
                GL.DeleteShader(vertexShader);
            }
            return program;
        }
    }
}
